# Calibrae Spawn

Calibrae Spawn is the initial base cryptocurrency node for the Calibrae social network system. It is both an initial, bitcoin-based cryptocurrency, as well as a funding vehicle for the Calibrae project.

## What is Calibrae

Calibrae will be at its base a similar type of monetised social network as [Steemit](https://steemit.com), but with full isolation between stake and reputation, with a base currency that will be forked from this code later to become an interest bearing Proof of Stake type currency, with an initial staking contract for accumulating interest at a rate of 5% per year.

## Social network

After this second phase of development, a social network system will be built following the same pattern as Steem, but with a secondary token type for each element of the protocol. There will be a coin for original posts, a coin for comments, and each protocol token will be tradeable on an internal exchange.

## Collaborative content production platform

Phase three will be to implement a Gitlab type protocol for, initially, hosting the code of the Spawn server, but completely open ended and intended to ultimately replace current centralised Git repository systems, but, just as the social network is monetised, and users vote by consuming and assigning reputation and from this calculating a distribution of new issued tokens, code will be voted on, and as well as serving as a governance system for the code, it will also pay the coder in code tokens, which will also be split into multiple types, for new code, code changes, issues, fixes, media content and documentation. The exact design is yet to be finalised.

## Privacy and Ecommerce

After this there will be implemented anonymous ZKP shielded transactions, a modular, programmable, monetised proxy routing system, and then an ecommerce platform which will also be secondarily monetised through a reputation based system that rates both buyers and sellers on their quality of service.

## Novel Data Replication and Distribution Protocol

The fundamental innovation will be in the expansion and refinement of the [Sporedb](https://gitlab.com/SporeDB/sporedb) decentralised database consensus protocol to form the basis of an extensible multi-data-type storage database system that will include everything from short text, long text, images, video, and software data types, with an intelligent sharding mechanism based on both data format, language as well as eventually a semantic AI shard distribution system that can be used to distribute data by short paths, by obfuscated paths, or to a global database, depending on the needs of the application.

## The economics of Calibrae

The Spawn server will implement a new token that will be named Nectar (NCTR), the staking contract will be called Stash, and the 5% per annum supply expansion of the token pool will serve as both an experiment in economics, to become a stable axis upon which other commodities can be reliably denominated, as well as, because of the exponential decay rate implied in this expansion of supply, serving as a crowd funding vehicle for the project itself.

## Why fork from btcd?

Initial efforts were directed at forking from Zclassic, which is barely altered from Zcash, and largely API compatible with Bitcoin. The tangle of the code, largely of course being from Bitcoin Core Team, was extremely difficult to navigate, and for the current sole developer of Calibrae, built on the hodge-podge, over-complex C++ language.

After previously having learned to work with Golang, the many irritating idiosyncracies and illogical patterns in C++ were unbearable. Not only that, but by choosing to work from a Golang codebase, which implements the Bitcoin protocol, cross platform implementation is facile, the build system is clean and consistent, and the language itself is very svelte and fast, not only in execution but also in compilation and extension of the code.

Furthermore, it is not intended that Calibrae Nectar is going to forever be a mineable currency, but by following closely the current widely understood protocol, will simplify integration with exchanges, it will incentivise the discovery of the project with the option of mining the currency, and with an accumulating userbase, become possible to in fact fund the listing of the currency on exchanges, a crucial step on the path towards securing funding to further develop Calibrae, by providing a lateral element besides the project's main goals.

Thus, by working from a reimplementation of Bitcoin's protocol, but written in a more approachable, and logical language, the bar of entry into the cryptocurrency market is lowered, and the modifications that will be made will be interesting to some segment of the cryptocurrency community especially those who are looking for something that is innovating in a new direction, and using modern development tools in the process that should also help accelerate the development of the network into something much bigger than the dreams of the original developer who has initiated the project.

_Pre-fork readme follows below_

---

[![Build Status](https://travis-ci.org/btcsuite/btcd.png?branch=master)](https://travis-ci.org/btcsuite/btcd)
[![ISC License](http://img.shields.io/badge/license-ISC-blue.svg)](http://copyfree.org)
[![GoDoc](https://img.shields.io/badge/godoc-reference-blue.svg)](http://godoc.org/github.com/btcsuite/btcd)

btcd is an alternative full node bitcoin implementation written in Go (golang).

This project is currently under active development and is in a Beta state. It
is extremely stable and has been in production use since October 2013.

It properly downloads, validates, and serves the block chain using the exact
rules (including consensus bugs) for block acceptance as Bitcoin Core. We have
taken great care to avoid btcd causing a fork to the block chain. It includes a
full block validation testing framework which contains all of the 'official'
block acceptance tests (and some additional ones) that is run on every pull
request to help ensure it properly follows consensus. Also, it passes all of
the JSON test data in the Bitcoin Core code.

It also properly relays newly mined blocks, maintains a transaction pool, and
relays individual transactions that have not yet made it into a block. It
ensures all individual transactions admitted to the pool follow the rules
required by the block chain and also includes more strict checks which filter
transactions based on miner requirements ("standard" transactions).

One key difference between btcd and Bitcoin Core is that btcd does _NOT_ include
wallet functionality and this was a very intentional design decision. See the
blog entry [here](https://blog.conformal.com/btcd-not-your-moms-bitcoin-daemon)
for more details. This means you can't actually make or receive payments
directly with btcd. That functionality is provided by the
[btcwallet](https://github.com/btcsuite/btcwallet) and
[Paymetheus](https://github.com/btcsuite/Paymetheus) (Windows-only) projects
which are both under active development.

## Requirements

[Go](http://golang.org) 1.8 or newer.

## Installation

#### Windows - MSI Available

https://github.com/btcsuite/btcd/releases

#### Linux/BSD/MacOSX/POSIX - Build from Source

* Install Go according to the installation instructions here:
  http://golang.org/doc/install

* Ensure Go was installed properly and is a supported version:

```bash
$ go version
$ go env GOROOT GOPATH
```

NOTE: The `GOROOT` and `GOPATH` above must not be the same path. It is
recommended that `GOPATH` is set to a directory in your home directory such as
`~/goprojects` to avoid write permission issues. It is also recommended to add
`$GOPATH/bin` to your `PATH` at this point.

* Run the following commands to obtain btcd, all dependencies, and install it:

```bash
$ go get -u github.com/Masterminds/glide
$ git clone https://github.com/btcsuite/btcd $GOPATH/src/github.com/btcsuite/btcd
$ cd $GOPATH/src/github.com/btcsuite/btcd
$ glide install
$ go install . ./cmd/...
```

* btcd (and utilities) will now be installed in `$GOPATH/bin`. If you did
  not already add the bin directory to your system path during Go installation,
  we recommend you do so now.

## Updating

#### Windows

Install a newer MSI

#### Linux/BSD/MacOSX/POSIX - Build from Source

* Run the following commands to update btcd, all dependencies, and install it:

```bash
$ cd $GOPATH/src/github.com/btcsuite/btcd
$ git pull && glide install
$ go install . ./cmd/...
```

## Getting Started

btcd has several configuration options available to tweak how it runs, but all
of the basic operations described in the intro section work with zero
configuration.

#### Windows (Installed from MSI)

Launch btcd from your Start menu.

#### Linux/BSD/POSIX/Source

```bash
$ ./btcd
```

## IRC

* irc.freenode.net
* channel #btcd
* [webchat](https://webchat.freenode.net/?channels=btcd)

## Issue Tracker

The [integrated github issue tracker](https://github.com/btcsuite/btcd/issues)
is used for this project.

## Documentation

The documentation is a work-in-progress. It is located in the [docs](https://github.com/btcsuite/btcd/tree/master/docs) folder.

## GPG Verification Key

All official release tags are signed by Conformal so users can ensure the code
has not been tampered with and is coming from the btcsuite developers. To
verify the signature perform the following:

* Download the Conformal public key:
  https://raw.githubusercontent.com/btcsuite/btcd/master/release/GIT-GPG-KEY-conformal.txt

* Import the public key into your GPG keyring:

  ```bash
  gpg --import GIT-GPG-KEY-conformal.txt
  ```

* Verify the release tag with the following command where `TAG_NAME` is a
  placeholder for the specific tag:
  ```bash
  git tag -v TAG_NAME
  ```

## License

btcd is licensed under the [copyfree](http://copyfree.org) ISC License.
