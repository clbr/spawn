# Notes about Spawn fork

These are notes being kept to track the things being changed to fork into Calibrae Spawn.

## [Server Ports](../params.go)

| Network       | Port  |
| ------------- | ----- |
| mainNet       | 2468  |
| regressionNet | 24680 |
| testNet3      | 24681 |
| simNet        | 24682 |
