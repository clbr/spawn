package main

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"
	"time"
)

const letterBytes = "0123456789abcdef"

// RandStringBytes generates an arbitrary hexdecimal string
func RandStringBytes(n int, seed int64) string {
	rand.Seed(seed)
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func main() {
	var seed = time.Now().UnixNano()
	var seedbytes = make([]byte, 8)
	binary.LittleEndian.PutUint64(seedbytes, uint64(seed))
	fmt.Print(RandStringBytes(130, seed), " ")
	log.Print(hex.EncodeToString(seedbytes))
}
