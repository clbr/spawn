# Genesis block for Calibrae Spawn

Using the link and text of the latest tweet from @mises, with the following commandline, the genesis block was generated

```
./gen `genkey/genkey` "https://twitter.com/mises/status/963179659570462721 It's YOUR #tax money. The government needs to stop the #unconstitutional spending on #NASA and #space exploration. Laurence M. Vance explains:"
```

Note that the genkey.go program generated the public key for the block using UnixNano(), as the seed.

Output of gen.c was as follows:

```
Coinbase: 010001041039653634646165616533646131323135

PubkeyScript: 412e404911c0d77d9fafe47c5f9d772d645b1d955a038d7b83f1c39b2f912e07e9de4f5cb510fc31d45db8a6a4f12b1b1a42b7fe7833691dd48f24647dcf61f29193ac

Merkle Hash: 33f289cc58df41187e5c66563cc9453460cf6199f528c12a23889bfc18eb03c5
Byteswapped: c503eb18fc9b88232ac128f59961cf603445c93c56665c7e1841df58cc89f233
Generating block...
2084110 Hashes/s, Nonce 129516188
Block found!
Hash: 00000000755e213fba390ee918aa112ae63fde5144435eff84520f55fc35a05f
Nonce: 130768328
Unix time: 1518516696
```
